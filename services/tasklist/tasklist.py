import functools
from flask import Flask, jsonify, request, make_response
import requests, json

database = 'tasklist'
collection = 'list'
database_url = 'http://database:5000'
default_database_params = {"database": database, "collection": collection}
json_headers = {'content-type': 'application/json'}

app = Flask(__name__)
app.debug = True


def error(message, code):
    return make_response(jsonify({"error": message}), code)


def check_params(params_get=None, params_post=None, params_delete=None, params_put=None):
    def __check_params(func):
        @functools.wraps(func)
        def check_params_inner(*args, **kwargs):
            
            if request.method == 'GET':
                for param in params_get:
                    if not param in request.args:
                        return error("incorrect GET input", 400)
            
            if request.method == 'POST':
                for param in params_post:
                    if not param in request.get_json().keys():
                        return error("incorrect POST input", 400)
            
            if request.method == 'DELETE':
                for param in params_delete:
                    if not param in request.get_json().keys():
                        return error("incorrect DELETE input", 400)
            
            if request.method == 'PUT':
                for param in params_put:
                    if not param in request.get_json().keys():
                        return error("incorrect PUT input", 400)
            
            return func(*args, **kwargs)

        return check_params_inner

    return __check_params


@app.route('/', methods=['GET', 'POST', 'DELETE'])
@check_params(params_get=['user'],
              params_post=['user', 'name'],
              params_delete=['user', 'name'])
def tasklist():
    if request.method == 'GET':
        user = request.args['user']
        return get_tasks_by_user(user)

    if request.method == 'POST':
        user = request.get_json()['user']
        name = request.get_json()['name']
        
        r = add_item(user, name)
        return r.json()

    if request.method == 'DELETE':
        user = request.get_json()['user']
        name = request.get_json()['name']
        r = database_request({"name": name, "user": user}, 'DELETE')
        return r.json()


@app.route('/done', methods=['POST'])
def set_done():
    user = request.json['user']
    name = request.json['name']
    done = request.json['done']
    app.logger.info(user + ' ' + name + ' ' + done)
    
    if change_done(user, name, done): 
        return jsonify({"status": "success"})
    else:
        return error("task not found", 404)


def add_item(user, name):
    item = get_task_by_name(user, name)
    if item:  # значит элемент уже есть в БД
        pass
    else:  # Добавление элемента
        r = database_request({"user": user, "name": name, "done": "false"}, 'POST')
    return r


def change_done(user, name, done):
    item = get_task_by_name(user, name)
    if item:
        if item['done'] == done:
            return True
        data = {}
        data['query'] = {"user": user, "name": name}
        data['data'] = {"done": done}
        r = database_request(data, 'PUT')
        return True
    else:
        return False


def get_task_by_name(user, name):
    """ Найти задачи пользователя с таким именем """
    r = database_request({"user": user, "name": name}, 'GET')
    
    if r.status_code == 400:
        error("incorrect params", 400)
    
    if r.status_code == 404:
        return None
    
    task = r.json()[0]
    return task


def get_tasks_by_user(user):
    """ Получить список всех задач пользователя """
    r = database_request({"user": user}, 'GET')
    app.logger.info(r.json())
    app.logger.info(r.status_code)
    
    if r.status_code == 404:
        return jsonify([])
    
    if r.status_code == 400:
        return error("incorrect params", 400)
    
    return jsonify(r.json())


def database_request(params, request_method):
    """ Сделать запрос к сервису БД """
    app.logger.info(params)
    
    if request_method == 'POST':
        data = [params]
        query = {"database": database, "collection": collection, "data": data}
        app.logger.info(query)
        r = requests.post(database_url, json=query, headers=json_headers)

    if request_method == 'GET':
        if not 'database' in params:
            params['database'] = database
        if not 'collection' in params:
            params['collection'] = collection
        r = requests.get(database_url, params=params, headers=json_headers)

    if request_method == 'PUT':
        if not 'query' in params or not 'data' in params:
            app.logger.error("data or query param for PUT request is empty")
            return None
        r = requests.put(database_url, json={"database":    database, 
                                             "collection":  collection,
                                             "query":       params['query'], 
                                             "data":        params['data']})

    if request_method == 'DELETE':
        data = [params]
        r = requests.delete(database_url, json={"database":     database, 
                                                "collection":   collection, 
                                                "data":         data})
    return r
