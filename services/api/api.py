from flask import Flask, jsonify, request, make_response
import functools
import requests
import json


app = Flask(__name__)
auth_url = "http://auth:5000"
tasklist_url = "http://tasklist:5000"
json_headers = {'content-type': 'application/json'}


def error(message, code):
    return make_response(jsonify({"error": message}), code)


def check_params(params_get=None, params_post=None, params_delete=None, params_put=None):
    def __check_params(func):
        @functools.wraps(func)
        def check_params_inner(*args, **kwargs):
            
            if request.method == 'GET':
                for param in params_get:
                    if not param in request.args:
                        return error("incorrect GET input", 400)
            
            if request.method == 'POST':
                for param in params_post:
                    if not param in request.get_json().keys():
                        return error("incorrect POST input", 400)
            
            if request.method == 'DELETE':
                for param in params_delete:
                    if not param in request.get_json().keys():
                        return error("incorrect DELETE input", 400)
            
            if request.method == 'PUT':
                for param in params_put:
                    if not param in request.get_json().keys():
                        return error("incorrect PUT input", 400)
            
            return func(*args, **kwargs)

        return check_params_inner

    return __check_params


def auth_needed(func):
    @functools.wraps(func)
    def check_auth(*args, **kwargs):
        if request.method == 'GET':
            app.logger.info('Wrapper GET request')
            if 'token' in request.args:
                token = request.args['token']
                user = check_auth_token(token)
                if user is None:
                    return error('invalid token', 400)
            else:
                return error('token not set', 400)

        elif request.method == 'POST':
            app.logger.info("Wrapper POST request")
            if 'token' in request.get_json():
                token = request.get_json()['token']
                user = check_auth_token(token)
                if user is None:
                    return error('invalid token', 400)
            else:
                return error('token not set', 400)
        return func(*args, **kwargs)

    return check_auth


def check_auth_token(token):
    """ Проверить токен, возвращает имя пользователя или None """
    r = requests.get(auth_url, params={"token": token})
    if 'user' in r.json():
        return r.json()['user']
    else:
        return None


def get_token(params):
    """ Получить токен авторизации по имени пользователя и паролю """
    r = requests.post(auth_url, json=params)
    if r.status_code == 200:
        return r.json()['token']
    else:
        return None


# POST: curl --header "Content-Type: application/json" --request POST --data '{ "user": "zaqwer101", "password": "1234"}' https://127.0.0.1/auth -k
@app.route('/auth', methods=['GET', 'POST'])
@check_params(params_get=['token'],
              params_post=['user'])
def auth():
    # проверяем токен авторизации
    if request.method == 'GET':
        token = request.args['token']
        user = check_auth_token(token)
        if user:
            return jsonify({"user": user})
        else:
            return error("invalid token", 401)

    # проверяем учетные данные и выдаём токен
    if request.method == 'POST':
        user = request.get_json()['user']
        params = {"user": user}
        if 'password_encrypted' in request.get_json():
            params['password_encrypted'] = request.get_json()['password_encrypted']
        elif 'password' in request.get_json():
            params['password'] = request.get_json()['password']
        else:
            error("no password provided", 400)
        token = get_token(params)
        if token is None:
            return error("invalid credentials", 401)
        return jsonify({"token": token})


@app.route('/tasklist', methods=['GET'])
@auth_needed
def tasklist_get_items():
    token = request.args['token']
    user = check_auth_token(token)

    r = requests.get(tasklist_url, params={"user": user})
    if r.status_code == 200:
        return jsonify(r.json())
    return error("incorrect input", 400)


@app.route('/tasklist', methods=['POST'])
@auth_needed
@check_params(params_post=['name'])
def tasklist_add_item():
    token = request.get_json()['token']
    user = check_auth_token(token)
    name = request.get_json()['name']

    params = {"user": user, "name": name, "done": "false"}
    
    r = requests.post(tasklist_url, json=params)
    if r.status_code == 200:
        return make_response(jsonify({"status": "success"}), 201)
    else:
        return r.json()


@app.route('/tasklist/done', methods=['POST'])
@auth_needed
@check_params(params_post=['name', 'done'])
def done():
    token = request.get_json()['token']
    user = check_auth_token(token)
    name = request.get_json()['name']
    done = request.get_json()['done']

    r = requests.post(f'{tasklist_url}/done',
                      json={"user": user, "name": name, "done": done})
    return r.json()


@app.route('/tasklist', methods=['DELETE'])
@auth_needed
@check_params(params_delete=['name'])
def tasklist_delete_item():
    token = request.get_json()['token']
    user = check_auth_token(token)
    name = request.get_json()['name']
    r = requests.delete(tasklist_url, json={'user': user, 'name': name})
    if r.status_code == 200:
        return jsonify({"status": "success"})
    return r.json()


@app.route('/register', methods=['POST'])
@check_params(params_post=['user', 'password'])
def register():
    user = request.get_json()['user']
    password = request.get_json()['password']
    r = requests.post(url=auth_url + '/register',
                      json={"user": user, "password": password})
    if r.status_code != 201: 
        return error(r.json()["error"], 400) 
    else:
        return r.json()
