from shared import *
import shared


@pytest.fixture(autouse=True)
def before():
    shared._before()


def test_add_success():
    """ добавление элемента в список. Успешные сценарии """
    token = register('test', 'testpassword')
    request('POST', '/tasklist', {"name": "task1", "token": token})
    data = get_tasklist_items(token)
    print(data)
    assert len(data) == 1
    assert data[0]['name'] == 'task1'
    assert data[0]['user'] == 'test'
    assert data[0]['done'] == 'false'


def test_delete_success():
    token = register('test', 'testpassword')
    request('POST', '/tasklist', {"name": "task1", "token": token})
    data = get_tasklist_items(token)
    assert data[0]['name'] == 'task1'

    request('DELETE', '/tasklist', {"name": "task1", "token": token})
    data = get_tasklist_items(token)
    assert len(data) == 0
    

def test_setdone_success():
    token = register('test', 'testpassword')
    request('POST', '/tasklist', {"name": "item1", "token": token})
    data = get_tasklist_items(token)
    assert data[0]['done'] == 'false'

    request('POST', '/tasklist/done', {"name": "item1", "token": token, "done": "true"})
    data = get_tasklist_items(token)
    assert data[0]['done'] == 'true'